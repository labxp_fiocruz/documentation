# Documento de visão
## Introdução

### Propósito:

Este artigo apresenta as justificativas para o projeto de ciência de dados, desenvolvido por um grupo de alunos da disciplina MAC0472 - Laboratório de Métodos Ágeis, em parceria com a Fiocruz, e visa auxiliar na compreensão do contexto em que esta ferramenta se inserirá. A partir deste propósito, a equipe procurará alcançar o desenvolvimento de uma solução tecnológica inovadora de gestão de notícias na área da saúde que possa trazer tecnologias emergentes para o foco da equipe de pesquisadores da Fiocruz.


### Escopo:

O documento de visão tem como objetivo informar aos envolvidos a problematização a ser resolvida e as funcionalidades do produto que serão atendidas pelo projeto da Fiocruz. Tendo em vista que este documento será atualizado durante a produção do sistema.

### Visão geral do documento:

O conteúdo deste documento está organizado de forma a fornecer: uma visão dos envolvidos no projeto; as necessidades enfrentadas; os benefícios desejados; e as funcionalidades do sistema face às suas características desenvolvidas para atender a estes benefícios. Também são apresentados aqui requisitos importantes para o produto final.

## Posicionamento

### Instrução do problema:

A equipe de dados da fiocruz já aplica Modelos de Processamento de Linguagem Natural para pesquisas de inovações na área da saúde em artigos científicos, eles tinham o objetivo de expandir esse escopo para outras fontes de informações criando modelos que fossem capaz de identificar tecnologias emergentes interessantes para eles em notícias.
 
Os clientes forneceram um dataset um pouco mais de 5000 notícias em inglês, das seguintes fontes 'Medical Xpress', 'The Bottom Line', 'Smithsonian Magazine','CMAJ News', 'Medical News Today', 'News Medical', 'RD World','Live Science', 'Futurity', 'Health Day', 'Nature','The Scientist', 'MIT News', 'Science Alert', 'IEEE Spectrum','SCI News', 'Science News', 'Broad Institute', 'World Pharma News','Institut Pasteur', 'Research Live', 'Wyss Institute','Tech News World'.
 
Os dados não haviam sido tratados, sendo necessária uma etapa de data cleaning e exploração do dataset. Em seguida foram estudados meios de utilizar modelos e algoritmos de machine learning e especificamente NPL para tentar classificar os principais tópicos das notícias e assim facilitar o trabalho dos cientistas.

### Instrução de posição do produto:

Tipo | Descrição
--------- | ------
Problema     | Impossibilidade de analisar descobertas interessantes no campo da saúde por falta de tempo de recursos.
Afeta    | Diretamente o trabalho dos pesquisadores que muitas vezes podem deixar passar alguma solução interessante para a organização por falta de recursos, e indiretamente impactam toda a população do país frente a importância do órgão para a saúde no Brasil.
Impactos    | Otimização dos inúmeros tempo dos pesquisadores e identificação de novas soluções
Solução  | Usar algoritmos em TF-IDF e LDA para resumir e filtrar notícias pelos tópicos de interesse em um determinado dataset.



## Visão Geral do Produto

###  Escopo do Produto
O objetivo desse produto é dar assistência aos profissionais da Fiocruz no processo de identificar quais são os tópicos que indicam possíveis áreas/projetos/tecnologias para investimento e estudo. 

### Propósito do produto
Com o objetivo de solucionar o problema exposto, a solução pensada propõe o desenvolvimento de um algoritmo capaz de identificar/calcular a importância de palavras e termos dentro de um determinado conjunto de textos fornecidos pelo usuário, para assim determinar quais são os principais temas presentes e servir de base estatística para o cliente poder analisar e investir ou não naquela área/projeto/tecnologia/etc.

###  Recursos do produto
* Remover caracteres especiais.
* Remover textos que não correspondem com a linguagem que está sendo analisada (inglês).
* Remoção de textos com tamanhos outliers com uma abordagem voltada ao percentil.
* Remoção de textos com o corpo contendo apenas um link para um vídeo.
* Exclusão de observações duplicadas.
* Exclusão dos textos ruídos contendo sintaxe de javascript
* Separar o dataset em 70% para treinar o modelo (train) e 30% para testes (test)
* Tokenização, com o objetivo de dividir o texto bruto em pequenos pedaços (tokens) para facilitar e otimizar o entendimento do contexto maior ao se usar PLN (processamento de linguagem natural).
* Lemmatização, com o objetivo de reduzir as palavras/tokens a seus radicais, determinando assim um lema. 
* Realizar cálculo do TF-IDF. Esse é um método de cálculo estatístico utilizado para entender a importância dos termos/tokens em um texto. 
* Utilização do algoritmo Latent Dirichlet Allocation (LDA) com o objetivo de encontrar categorias no conjunto de tokens e agrupá-los.

## Descrição das partes envolvidas

### Perfis das Partes Interessadas
* Clientes: Fiocruz (Fundação Oswaldo Cruz).
* Usuários: Equipe de Ciências e Engenharia de Dados da Fiocruz.
* Equipe: É composta por graduandos e pós-graduandos em Ciências da Computação e Sistemas de Informação pela Universidade de São Paulo, discentes das disciplinas de Laboratório de Metodologias Ágeis.

### Perfis do Usuário
Os usuários serão os funcionários da área de ciências e engenharia de dados da instituição de saúde contratante.

## Restrições

* Descrição geral da bibliotecas necessárias no [Documento de Arquitetura](https://gitlab.com/labxp_fiocruz/documentation/-/blob/dev/architecture/architecture.md), usando o ambiente de desenvolvimento em docker não é necessária a instalação de bibliotecas;
 
* Necessidade de algum compilador de *Pyhton3*;
 
* Necessário Programa para rodar jupyter notebooks - *jupyter, JupyterLab, collab*.
 
* O processo de tokenização é um pouco custoso computacionalmente - o uso de hardware específico(GPU) seria o mais indicado, mas é possível rodar as soluções em CPUs convencionais;
 
* Entrega do produto final para dia 16/12/2021.


## Referências
Biblioteca **spaCy** para processamento avançado de linguagem natural. Disponível em: https://spacy.io/

Biblioteca **scikit-learn** para aplicação prática de machine learning. Disponível em: https://scikit-learn.org/stable/

SATO, Danilo; WIDER, Arif; WINDHEUSER, Christoph. **Continuous Delivery for Machine Learning**. Disponível em: https://martinfowler.com/articles/cd4ml.html#TestingAndQualityInMachineLearning

SCOTT, William. **TF-IDF from scratch in python on a real-world dataset**. Disponível em: https://towardsdatascience.com/tf-idf-for-document-ranking-from-scratch-in-python-on-real-world-dataset-796d339a4089

DAS, Bijoyan. **Understanding TF-IDF Model - Hands On NLP using Python Demo**. Disponível em: https://www.youtube.com/watch?v=ouEVPRMHR1U&t=620s
