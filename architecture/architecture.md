# Documento de arquitetura
## Representação das Relações

[<img src="diagrams/representacao_relacoes.png" width="1300" title="">]()

O diagrama apresenta cada etapa que será seguido para que o projeto funcione. Relacionando e organizando as relações. É possível notar a classe X em azul, que aplica o padrão de projeto Façade do GoF. Esse é a interface que cria uma fachada de interação com o sistema.

### Interações interna do projeto


## Metas Restrições de Arquitetura

Para o desenvolvimento deste projeto serão ultilizadas as seguintes tecnologias e restrições:

- [Docker](https://www.docker.com) Utilizado para conteinerização do ambiente de desenvolvimento
- [Jupyter Notebook](https://jupyter.org/) Notebook utilizado para facilitar na construção e na visualização dos códigos 
- [Matplotlib](https://matplotlib.org/) Biblioteca utilizada para criação de gráficos e interações visuais
- [Python](https://www.python.org/) Linguagem utilizada para desenvolver
- [Pytest](https://docs.pytest.org/en/6.2.x/contents.html) Ferramenta utilizada para ajudar a testar os programas
- [Pandas](https://pandas.pydata.org) Biblioteca utilizada no projeto para manipulação e análise dos dados
- [spaCy](https://spacy.io/) Biblioteca utilizada para modelar a visualizar a análise de dados
- [scikit-learn](https://scikit-learn.org/stable/) Ferramenta para análise de dados preditivo


## Diagrama de Pacotes

[<img src="diagrams/diagrama_pacotes.png" width="1300" title="">]()

O projeto SITH está separado em quatro diretórios principais, são eles:

* **data**
* **notebooks**
* **src**
* **test**

No **src** se encontram os seguintes arquivos:

* **SITHAPI.py** - interface de programação que acessa o conjunto de dados de treinamento que está na pasta data e chama os outros módulos;
* **Cleaner.py** - acessa o conjunto de dados que está na pasta data, aplica uma limpeza nos dados e salva as modificações;
* **Splitter.py** -  acessa o conjunto de dados e o divide em dados de treinamento e dados de teste de forma estratificada e por fim salva esses conjuntos em arquivos diferentes; 
* **PreProcessor.py** - módulo para o Pré-processamento de dados;
* **utils.py** - módulo com funções e variáveis de comum acesso pelo outros módulos; 
* **Tfidf.py** - módulo que gera um modelo de tf-idf;

No **test** se encontram os seguintes arquivos:

* **test_Cleaner.py** - teste de unidade para a biblioteca **Cleaner.py**;
* **test_Splitter.py** - teste de unidade para a biblioteca **Splitter.py**;
* **test_PreProcessor.py** - teste de unidade para a biblioteca **PreProcessor.py**;
* **test_utils.py** - teste de unidade para o **utils.py**;
* **test_Tfidf.py** - teste de unidade para o **Tfidf.py**;

No **data** se encontram os nossos conjuntos de dados.

No **notebooks** se encontram todos os notebooks criados ao longo do desenvolvimento do projeto.

## Pipeline dos dados

[<img src="diagrams/diagrama_dados.png" width="1300" title="">]()

No diagrama acima, temos uma ilustração da pipeline dos dados, com breves descrições sobre quais passos são executados por cada classe.

### Testes
 
Este artigo apresenta as justificativas, o modo de execução e cobertura dos testes para o projeto SITH de ciência de dados, desenvolvido por um grupo de alunos da disciplina MAC0472 - Laboratório de Métodos Ágeis, em parceria com a Fiocruz, e visa auxiliar na compreensão do contexto em que esta ferramenta se inserirá.
 
Os testes foram usados para fazer integração contínua das bibliotecas de limpeza e processamento dos dados. Os testes foram implementados com o módulo pytest.
 
### Tipo de teste e execução
 
Os principais testes implementados foram teste de unidade que testam se um determinada função está funcionando corretamente, para tanto, foi passada um entrada e a saída esperada e algoritmo testa se a função está devolvendo a saída esperada.
 
os testes são execultados de maneira automática pela Gitlab.CI toda vez que uma nova funcionalidade for passada para o gitlab e um commit for realizado, mas esses testes também podem ser executados se de dentro da pasta **src** dos testes for executado o comando pytest.
 
```
.\Sith\test\src $- pytest
```
 
### Funcionalidades Testadas
 
Foram desenvolvidos teste automatizados para três bibliotecas: **Cleaner, Utils** e **PreProcessor**.
 
No arquivo de limpeza foram testada as seguintes funcionalidades:
 
* **broken_line** - Testa a funcionalidade que valida se não houve nenhuma quebra de linha indevida;
* **value_duplicated** - Testa se não há teste duplicados;
* **remove_JS** - Testa se não há código em Javascript;
* **remove_language** - Remove caracteres asiáticos;
* **remove_outliers_by_percentile** - Testa a função que remove outliers, textos muito pequenos ou textos muito grandes;
* **clean_dataset** - Testa todas as funcionalidades aplicadas juntas.
 
Para cada teste foi criado um pequeno dataframe sujo e um novo com o valor esperado para testar se a função está funcionando corretamente.
 
No Utils foram testadas duas funcionalidades:
 
* **remove_special_caracter** - Remove caracteres especiais que possam quebrar o csv de maneira indevida.
* **clean_broke_line** - Testa se a linha não está sendo quebrada de maneira indevida.
 
Por fim, no PreProcessor foi testado as seguintes funcionalidades:
 
* **tokenize_single_text_remove_stopwords** - Testa a tokenização retirando apenas palavras sem significado, conjunções...;
* **tokenize_single_text_remove_punctuation** - Não gera token de pontuação;
* **tokenize_single_text_remove_number** - Não gera token de números;
* **tokenize_single_text_let_number** - Deixa os números virarem tokens;
* **tokenize_single_text_remove_ascii** - Remove tokens que tenham caracteres que não sejam ascii;
* **tokenize_single_text_pos_verb** - Deixa apenas os tokens de verbos;
* **tokenize_single_text_pos_noun** - Deixa apenas os tokens de substantivos.
 
Esses testes foram elaborados passando pequenos textos para a função e testando com a saída esperada.

### Cobertura

Para avaliar a cobertura de testes foi utlizada a biblioteca **pytest-cov**, que gera um relatório sobre a cobertura do pytest. Para gerar esse relatório basta executar de dentro da pasta **src** dos testes o comando pytest --cov=../../src

```
.\Sith\test\src $- pytest --cov=../../src
```
```
---------- coverage: platform linux, python 3.7.10-final-0 -----------
Name                                                                                      Stmts   Miss  Cover
-------------------------------------------------------------------------------------------------------------
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/cleaning_split.py          50     17    66%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/text_preprocessing.py      29      7    76%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/tf_idf.py                  18     18     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/utils.py                   21      3    86%
-------------------------------------------------------------------------------------------------------------
TOTAL                                                                                        118     45    62%
```

A cobertura ficou um pouco baixa pois no arquivo com o modelo em si não poderia ser aplicados testes de unidades pela variação dos resultados. Mas no geral todos os outros arquivos tiveram mais 60% de cobertura.

No final fizemos uma adaptação para transformar os arquivos com funções em classes e métodos, isso reduziu a corbetura pelo pytest, mas as funções se mantiveram as mesma, portanto consideramos que cobrimos uma quantidade suficiente de funções, só que a mudança no formato fez com que o pytest não fizesse corretamente o cálculo da cobertura.

```
----------- coverage: platform linux, python 3.8.5-final-0 -----------
Name                                                                                      Stmts   Miss  Cover
-------------------------------------------------------------------------------------------------------------
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/Cleaner.py                 25      0   100%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/DataLoader.py              21     10    52%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/PreProcessor.py            33     33     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/Splitter.py                10     10     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/Tfidf.py                   32     32     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/__init__.py                46     46     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/cleaning_split.py          50     17    66%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/text_preprocessing.py      29      7    76%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/tf_idf.py                  18     18     0%
/home/daniel/Documentos/IME/4sem/metodosAgeis/experimentation/src/utils.py                   23      3    87%
-------------------------------------------------------------------------------------------------------------
TOTAL                                                                                       287    176    39%
```

