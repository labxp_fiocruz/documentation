# SITH Documentação

`SITH Documentação` é o repositório que apresenta a documentação envolvendo a arquitetura e visão de produto do projeto de análise de dados desenvolvido por alunos da disciplina de Laboratório de Métodos Ágeis fornecida pelo instituto IME na Universidade de São Paulo (USP) tendo como cliente os pesquisadores da Fiocruz. Todos os materiais de desenvolvimento da aplicação podem ser acessados no repositório [SITH](https://gitlab.com/labxp_fiocruz/experimentation). 

[<img src="images/sith.png" width="500" title="SITH Logo" align="center">]()

## Quer contribuir?

Gostou do SITH e gostaria de contribuir com ele? Acesse o [guia de contruição](https://gitlab.com/labxp_fiocruz/documentation/-/blob/dev/CONTRIBUTING.md) para saber como pode contribuir com o projeto.

## Autores

- Daniel Silva Lopes da Costa
- Daniella Fernanda Cisterna Melo
- Gabriela Jie Han
- Isaque Alves de Lima
- Leonardo Martinez Ikeda
- Victor Senoguchi Borges

## Licença

Este projeto adota a [licença MIT](https://opensource.org/licenses/MIT) (criada pelo  Instituto de Tecnologia de Massachusetts)

