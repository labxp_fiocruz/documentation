# Guia de Contribuição

## Politica de Issues

 - As issues devem possuir título, descrição, no mínimo um assinante responsável, e labels representando o tipo de trabalho que está sendo executado.

## Politica de Branchs

#### main

A branch **main** é a branch de produção, onde ficará a versão estável do projeto. Ela estará bloqueada para commits e para pushs. (Veja a política de merges no tópico Merges para main)

#### dev

A branch **dev** é a branch de homologação/desenvolvimento, onde ficará a versão de desenvolvimento do projeto. (Essa branch não apresenta a versão mais estável do projeto, mas pode ser utilizada para validações com o cliente)

#### branchs de features

As branchs de desenvolvimento de features serão criadas a partir da branch master com a nomenclatura padrão **x_nome_da_issue**, onde o x representa o código de rastreio da issue.

## Política de Commits

Os commits devem ser feitos usando o parâmetro `-s` para indicar sua assinatura no commit.

```
git commit -s
```
A issue em questão deve ser citada no commit, para isso, basta adicionar `#<numero_da_issue>`.

```
 #42 Fazendo guia de contribuição
```

** \*\*Por padrão, o caracter `#` define uma linha de comentário no arquivo da mensagem do commit. Para resolver este problema, use o commando:**
```
git config --local core.commentChar '!'
```

Igualmente, para commits em dupla deve ser usado o comando `-s` , e deve ser adicionado a assinatura da sua dupla.

```
git commit -s
```
Comentário do commit:
```
#12 Fazendo guia de contribuição
- inserindo politica de branch
- inserindo politica de commit
Signed-off-by: Isaque Alves's avatarIsaque Alves <isaquealvesdl@gmail.com>
```

Para commits que encerram a resolução de uma issue, deve-se iniciar a mensagem do commit com `Fix #<numero_da_issue> <mensagem>`, para que a issue seja [encerrada automaticamente](https://help.github.com/articles/closing-issues-using-keywords/) quando mesclada na `master`.

Exemplo de comentário do commit:
```
Fix #12 Finalizando guia de contribuição do projeto
```

Para commits que incluem uma pequena mudança em uma issue que já teve sua resolução encerrada, deve-se iniciar a mensagem do commit com `HOTFIX #<numero_da_issue> <mensagem>`

Exemplo de comentário do commit:
```
HOTFIX #12 Atualizando guia de contribuição do projeto
```
## Politica de Merge

Deve ser feito para a dev quando alguma funcionalidade for implementada. 

Os merges para main deverão ser feitos quando a funcionalidade ou refatoração estiverem de acordo com os seguintes aspectos:

- Funcionalidade ou refatoração concluída;
- Progredir ou manter a porcentagem de cobertura de teste;
- Funcionalidade revisada por algum outro membro (caso necessário).

